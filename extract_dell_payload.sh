#!/usr/bin/env bash
#
# extract_dell_payload (c) 2018 troyengel
# SPDX-License-Identifier: MIT
#
# Extract the payload file(s) from a Dell firmware .BIN download
# for Linux, which can then be flashed via the iDRAC webUI
#
# The tar.gz payload starts one line after a defined marker line
# - extracting from there to end of file should be a valid tgz

_VERSION="0.0.1"
_NAME=$(basename ${0})

# This appears to be consistent to all .BIN files
_TGZ_MARKER="#####Startofarchive#####"

# Default output directory
_DEFOUT="./payload"

# generic help output
function usage() {
  echo "$_NAME version $_VERSION"
  echo
  echo "Extract the payload file(s) from a Dell firmware .BIN download"
  echo
  echo "Usage: $_NAME < -f FILENAME > [ -o OUTDIR ] [ -h ] [ -V ]"
  echo
  echo "  -f FILENAME   Name of downloaded .BIN file from Dell"
  echo "  -o OUTDIR     Output directory, default: ${_DEFOUT}"
  echo "  -V            Version of program"
  echo "  -h            This help text"
  echo
}

# get the user inputs
while getopts ":f:o:Vh" opt; do
  case "$opt" in
    f)
      BINFILE="$(realpath ${OPTARG})" ;;
    o)
      OUTDIR="$(realpath ${OPTARG})" ;;
    V)
      echo "$_NAME version $_VERSION"
      exit 0 ;;
    h)
      usage
      exit 0 ;;
    *)
      echo "Unrecognized option: $OPTARG (Run '$_NAME -h' for help)"
      exit 1 ;;
  esac
done
shift $((OPTIND-1))

# ensure we have an input file and can read it and use it's full path
if [[ -z "${BINFILE}" ]]; then
  echo "Error: -f FILENAME is required"
  usage
  exit 2
elif [[ ! -r "${BINFILE}" ]]; then
  echo "Error: cannot open file ${BINFILE}"
  usage
  exit 3
fi

# create desired output directory if supplied
_OUT="${_DEFOUT}"
if [[ -n "${OUTDIR}" ]]; then
  _OUT="${OUTDIR}"
fi
if [[ ! -d "${_OUT}" ]]; then
  mkdir -p "${_OUT}"
fi
_ARGS="-C ${_OUT}"

# where does the payload start (offset)
_TGZ_LINE=$(grep -m1 -an "^$_TGZ_MARKER" "${BINFILE}" | cut -d ":" -f 1)
_TGZ_START=$((_TGZ_LINE+1))

# extract tarball starting at marker
tail -n +${_TGZ_START} "${BINFILE}" | tar ${_ARGS} -zvxf - \
  --strip=1 --wildcards --no-anchored "payload/*"

# print out a md5sum of the files just extracted
md5sum "${_OUT}/"*

